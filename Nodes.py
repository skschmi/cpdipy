# Python implementation of CPDI
# Steven Schmidt
# December 2013

import numpy

class Nodes():

    def __init__(self,domainxmin,domainxmax,nnodes):
        self.nnodes = nnodes
        self.hx = (domainxmax - domainxmin)/(nnodes-1)
        self.x = numpy.linspace(domainxmin,domainxmax,nnodes)
        self.q = numpy.zeros((nnodes))
        self.m = numpy.zeros((nnodes))
        self.volume = numpy.zeros((nnodes))
        self.fint = numpy.zeros((nnodes))
        self.fext = numpy.zeros((nnodes))
        self.dqdt = numpy.zeros((nnodes))

    def xmin(self):
        return self.x[0]

    def xmax(self):
        return self.x[-1]

    # Nodal basis function
    def shapef(self,i,x):
        xdom0 = self.x[numpy.mod((i-1),self.nnodes)]
        xdom1 = self.x[i]
        xdom2 = self.x[numpy.mod((i+1),self.nnodes)]
        leftelbool = numpy.logical_and(numpy.greater(x,xdom0), numpy.less(x,xdom1))
        rightelbool = numpy.logical_and(numpy.greater(x,xdom1), numpy.less(x,xdom2))
        midpointbool = numpy.equal(x,xdom1)
        return leftelbool*(x - xdom0)/(xdom1-xdom0) + rightelbool*(x-xdom2)/(xdom1-xdom2) + midpointbool*1.0

    # d/dx of nodal basis function
    def del_shapef(self,i,x):
        xdom0 = self.x[numpy.mod((i-1),self.nnodes)]
        xdom1 = self.x[i]
        xdom2 = self.x[numpy.mod((i+1),self.nnodes)]
        leftelbool = numpy.logical_and(numpy.greater(x,xdom0), numpy.less_equal(x,xdom1))
        rightelbool = numpy.logical_and(numpy.greater(x,xdom1), numpy.less_equal(x,xdom2))
        midpointbool = numpy.equal(x,xdom1)
        return leftelbool/(xdom1-xdom0) + rightelbool/(xdom1-xdom2) + midpointbool/(xdom1-xdom0)
