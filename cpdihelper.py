# Python implementation of CPDI
# Steven Schmidt
# December 2013

import numpy
from numpy import linalg
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot


# Left-side General Basis Function
def leftbasisf(x0,x1,x):
    return (x - x1)/(x0-x1)

# Right-side General Basis Function
def rightbasisf(x0,x1,x):
    return (x - x0)/(x1-x0)

# Left-side derivative of basis function
def del_leftbasisf(x0,x1,x):
    return 1./(x0-x1)

# Right-side derivative of basis function
def del_rightbasisf(x0,x1,x):
    return 1./(x1-x0)


##########################
# Modified Shape functions
##########################
# Let x be a length-m array
# Let there be n particles
# X: n-by-m matrix, each column containing the x value
# LCOR: n-by-m matrix, each row containing the left-corner x value of a particle
# RCOR: n-by-m matrix, each row containing the right-corner x value of a particle
# Lfvals: n-by-m matrix, each row containing the modified-shape-function value evaluated at a left-corner x value
# Rfvals: n-by-m matrix, each row containing the modified-shape-function value evaluated at a right-corner x value
# InPart: n-by-m matrix. Row: particle. Column: x value.  Value: True if x is in particle domain, False otherwise.
#----
# nodes:  nodes object
# parts: particles object
# i: Index to shape function (scalar)
# x: Array of x values to evaluate the function
# Returns array of function evaluations at the x value locations
def modshapef(nodes,parts,i,x):
    X,LCOR = numpy.meshgrid(x,parts.corner(0))
    X,RCOR = numpy.meshgrid(x,parts.corner(1))
    Lfvals = nodes.shapef(i,LCOR)
    Rfvals = nodes.shapef(i,RCOR)
    InPart = numpy.logical_and(numpy.greater(X,LCOR), numpy.less_equal(X,RCOR))
    InPart[0,0] = ((X[0,0] >= LCOR[0,0]) and (X[0,0]<=RCOR[0,0]))
    ANS = InPart*(Lfvals*leftbasisf(LCOR,RCOR,X)+Rfvals*rightbasisf(LCOR,RCOR,X))
    return ANS.sum(axis=0)

def del_modshapef(nodes,parts,i,x):
    X,LCOR = numpy.meshgrid(x,parts.corner(0))
    X,RCOR = numpy.meshgrid(x,parts.corner(1))
    Lfvals = nodes.shapef(i,LCOR)
    Rfvals = nodes.shapef(i,RCOR)
    InPart = numpy.logical_and(numpy.greater(X,LCOR), numpy.less_equal(X,RCOR))
    InPart[0,0] = ((X[0,0] >= LCOR[0,0]) and (X[0,0]<=RCOR[0,0]))
    ANS = InPart*(Lfvals*del_leftbasisf(LCOR,RCOR,X)+Rfvals*del_rightbasisf(LCOR,RCOR,X))
    return ANS.sum(axis=0)

# Inputs:
# nodes:  nodes object
# parts: particles object
# Returns a matrix of phiip constants for each node and particle
# Returns matrix:
#   Rows: nodes
#   Columns: particles
def phiip(nodes,parts):
    iv = numpy.arange(0,nodes.nnodes)
    LCOR,IM = numpy.meshgrid(parts.corner(0),iv)
    RCOR,IM = numpy.meshgrid(parts.corner(1),iv)
    Lfvals = nodes.shapef(IM,LCOR)
    Rfvals = nodes.shapef(IM,RCOR)
    ANS = 0.5*(Lfvals+Rfvals)
    return ANS

def del_phiip(nodes,parts):
    iv = numpy.arange(0,nodes.nnodes)
    LCOR,IM = numpy.meshgrid(parts.corner(0),iv)
    RCOR,IM = numpy.meshgrid(parts.corner(1),iv)
    Lfvals = nodes.del_shapef(IM,LCOR)
    Rfvals = nodes.del_shapef(IM,RCOR)
    ANS = 0.5*(Lfvals+Rfvals)
    return ANS

#Particle Corner mapping matrix
def cornerPhi(nodes,parts):
    iv = numpy.arange(0,nodes.nnodes)
    COR,IM = numpy.meshgrid(parts.corners,iv)
    fvals = nodes.shapef(IM,COR)
    return fvals

# Calculate an appropriate timestep
def calcTimeStep(parts,nodes,wavespeed,cflfrac):
    maxPartVel = (parts.q/parts.m).max()
    timestep = cflfrac*nodes.hx/(wavespeed + maxPartVel)
    return timestep

######################
# CONSERVATION CHECKS:
######################

def totalRecoveredMomentum(nodes,parts,h):

    ### FIRST, we recover the density field.
    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),h)
    densityrecovfunc = numpy.zeros(pltx.size)

    # NOTE: nzv means "non zero volume"
    nzvi, = numpy.nonzero(nodes.volume)
    for i in nzvi:
        modshpf = modshapef(nodes,parts,i,pltx)
        densityrecovfunc[:] += (nodes.m[i]/nodes.volume[i])*modshpf


    ## THEN we recover the velocity field
    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),h)
    velrecovfunc = numpy.zeros(pltx.size)

    # NOTE: nzm means "non zero mass"
    nzmi, = numpy.nonzero(nodes.m)
    for i in nzmi:
        modshpf = modshapef(nodes,parts,i,pltx)
        velrecovfunc[:] += (nodes.q[i]/nodes.m[i])*modshpf

    #return recovfunc.sum()*h
    return (densityrecovfunc*velrecovfunc).sum()*h


#####################
# RECOVERED FUNCIONS
#####################

def recoveredVelocityField(nodes,parts,x):
    velrecovfunc = numpy.zeros(x.size)

    # Regular Nodes
    # NOTE: nzm means "non zero mass"
    nzmi, = numpy.nonzero(nodes.m[0:nodes.nnodes])
    for i in nzmi:
        modshpf = modshapef(nodes,parts,i,x)
        velrecovfunc[:] += (nodes.q[i]/nodes.m[i])*modshpf

    return velrecovfunc


def recoveredMassDensityField(nodes,parts,x):
    recovfunc = numpy.zeros(x.size)

    # Regular Nodes
    # NOTE: nzv means "non zero volume"
    nzvi, = numpy.nonzero(nodes.volume[0:nodes.nnodes])
    for i in nzvi:
        modshpf = modshapef(nodes,parts,i,x)
        recovfunc[:] += (nodes.m[i]/nodes.volume[i])*modshpf

    return recovfunc


def recoveredMomentumDensityField(nodes,parts,x):
    ### FIRST, we recover the density field.##
    densityrecovfunc = recoveredMassDensityField(nodes,parts,x)
    ## THEN we recover the velocity field ##
    velrecovfunc = recoveredVelocityField(nodes,parts,x)
    ## Now we calculate the momentum density field
    recovfunc = densityrecovfunc*velrecovfunc
    return recovfunc


def recoveredMassPseudofield(nodes,parts,x):

    recovfunc = numpy.zeros(x.size)

    # Regular nodes
    # NOTE: nzm means "non zero mass"
    nzmi, = numpy.nonzero(nodes.m[0:nodes.nnodes])
    for i in nzmi:
        modshpf = modshapef(nodes,parts,i,x)
        recovfunc[:] += (nodes.m[i])*modshpf

    return recovfunc


def recoveredMomentumPseudofield(nodes,parts,x):

    recovfunc = numpy.zeros(x.size)

    # Regular nodes
    for i in xrange(0,nodes.nnodes):
        modshpf = modshapef(nodes,parts,i,x)
        recovfunc[:] += (nodes.q[i])*modshpf

    return recovfunc


def recoveredVolumePseudofield(nodes,parts,x):

    recovfunc = numpy.zeros(x.size)

    # Regular nodes
    for i in xrange(0,nodes.nnodes):
        modshpf = modshapef(nodes,parts,i,x)
        recovfunc[:] += (nodes.volume[i])*modshpf

    return recovfunc


#######
# PLOTS
#######

def plotShapeFunctions(nodes,parts,newfig=True,titlestr="Shape Functions"):
    # Plot start
    if(newfig):
        pyplot.figure()

    # Plot the particles
    partploty = -(parts.xmax()-parts.xmin())*0.04*numpy.ones((parts.nparts+1))
    partplotx = parts.corners
    pyplot.plot(partplotx,partploty,'b*')

    # plot the nodes
    nodeploty = numpy.zeros(nodes.nnodes)
    nodeplotx = nodes.x
    pyplot.plot(nodeplotx,nodeploty,'g*')

    # plot the nodal shape functions
    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),0.001)
    for i in xrange(0,nodes.nnodes):
        pyplot.plot(pltx,nodes.shapef(i,pltx),'k')

    # plot the particle shape functions
    #for i in xrange(0,parts.nparts+1):
    #    pyplot.plot(nshplotx,parts.shapef(i,nshplotx),'b')

    # plot the modified shape functions
    modshpfSUM = numpy.zeros(pltx.size)
    for i in xrange(0,nodes.nnodes):
        modshpf = modshapef(nodes,parts,i,pltx)
        modshpfSUM[:] += modshpf[:]
        pyplot.plot(pltx,modshpf,linewidth=2)

    # Plot the sum of everything (to show partition of unity)
    pyplot.plot(pltx,modshpfSUM,color='y',linewidth=2)

    pyplot.xlim([nodes.xmin(),nodes.xmax()])
    pyplot.ylim([-0.5,1.5])

    pyplot.title(titlestr)


def plotVelocity(nodes,parts,h,newfig=True):

    if(newfig):
        pyplot.figure()

    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),h)

    velrecovfunc = recoveredVelocityField(nodes,parts,pltx)

    pyplot.plot(pltx,velrecovfunc,linewidth=2,color='r')

    # Regular Nodes
    nzmi, = numpy.nonzero(nodes.m[0:nodes.nnodes])
    velVals = numpy.zeros(nodes.nnodes)
    velVals[nzmi] = (nodes.q[nzmi]/nodes.m[nzmi])
    pyplot.plot(nodes.x,velVals,'r*')

    # Particle velocities
    pyplot.plot(pltx,parts.stairstepfforvalue(parts.q/parts.m,pltx),'b')
    pyplot.plot(parts.x,(parts.q/parts.m),'b*')

    #Title
    pyplot.title("Velocity")


def plotMassDensity(nodes,parts,h,newfig=True):
    if(newfig):
        pyplot.figure()

    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),h)

    recovfunc = recoveredMassDensityField(nodes,parts,pltx)

    pyplot.plot(pltx,recovfunc,linewidth=2,color='g')

    # Regular nodes
    nzvi, = numpy.nonzero(nodes.volume[0:nodes.nnodes])
    densVals = numpy.zeros(nodes.nnodes)
    densVals[nzvi] = (nodes.m[nzvi]/nodes.volume[nzvi])
    pyplot.plot(nodes.x,densVals,'g*')

    # Particle mass-densities
    pyplot.plot(pltx,parts.stairstepfforvalue((parts.m/parts.volume()),pltx),'b')
    pyplot.plot(parts.x,(parts.m/parts.volume()),'b*')

    #Title
    pyplot.title("Mass-Density\n(mass/volume)")


def plotMomentumDensity(nodes,parts,h,newfig=True):

    if(newfig):
        pyplot.figure()

    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),h)
    recovfunc = recoveredMomentumDensityField(nodes,parts,pltx)
    pyplot.plot(pltx,recovfunc,linewidth=2,color='r')

    # Regular nodes
    nzvi, = numpy.nonzero(nodes.volume[0:nodes.nnodes])
    qdensVals = numpy.zeros(nodes.nnodes)
    qdensVals[nzvi] = (nodes.m[nzvi]/nodes.volume[nzvi])*(nodes.q[nzvi]/nodes.m[nzvi])
    pyplot.plot(nodes.x,qdensVals,'r*')

    # Particle momentum-densities
    parts_qdens = (parts.m/parts.volume())*(parts.q/parts.m)
    pyplot.plot(pltx,parts.stairstepfforvalue(parts_qdens,pltx),'b')
    pyplot.plot(parts.x,parts_qdens,'b*')

    # Title
    pyplot.title("Momentum-Density\n(density*velocity) or (m/v)*(q/m) or (q/v)")


def plotMomentumWRONG(nodes,parts,h,newfig=True):

    if(newfig):
        pyplot.figure()

    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),h)
    recovfunc = recoveredMomentumPseudofield(nodes,parts,pltx)

    pyplot.plot(pltx,recovfunc,linewidth=2,color='r')

    # Regular nodes
    qVals = numpy.zeros(nodes.nnodes)
    qVals[:] = nodes.q[0:nodes.nnodes]
    pyplot.plot(nodes.x,qVals,'r*')

    # Particle momentums
    pyplot.plot(pltx,parts.stairstepfforvalue(parts.q,pltx),'b')
    pyplot.plot(parts.x,parts.q,'b*')

    # Title
    pyplot.title("Momentum Pseudo-Field (q)")


def plotMassWRONG(nodes,parts,h,newfig=True):
    if(newfig):
        pyplot.figure()

    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),h)
    recovfunc = recoveredMassPseudofield(nodes,parts,pltx)

    pyplot.plot(pltx,recovfunc,linewidth=2,color='c')

    # Regular nodes
    # NOTE: nzm means "non zero mass"
    nzmi, = numpy.nonzero(nodes.m[0:nodes.nnodes])
    massVals = numpy.zeros(nodes.nnodes)
    massVals[nzmi] = (nodes.m[nzmi])
    pyplot.plot(nodes.x,massVals,'c*')

    # Particle masses
    pyplot.plot(pltx,parts.stairstepfforvalue(parts.m,pltx),'b')
    pyplot.plot(parts.x,(parts.m),'b*')

    # Title
    pyplot.title("Mass Pseudo-Field (m)")


def plotVolumeWRONG(nodes,parts,h,newfig=True):

    if(newfig):
        pyplot.figure()

    pltx = numpy.arange(nodes.xmin(),nodes.xmax(),h)
    recovfunc = recoveredVolumePseudofield(nodes,parts,pltx)

    pyplot.plot(pltx,recovfunc,linewidth=2,color='y')

    # Regular nodes
    volVals = numpy.zeros(nodes.nnodes)
    volVals[:] = nodes.volume[0:nodes.nnodes]
    pyplot.plot(nodes.x,volVals,'r*')

    # Particle momentums
    pyplot.plot(pltx,parts.stairstepfforvalue(parts.volume(),pltx),'b')
    pyplot.plot(parts.x,parts.volume(),'b*')

    # Title
    pyplot.title("Volume Pseudo-Field (volume)")


##########
# OTHER
##########

def mapNodesToParticles_INV_TEST_2(nodes,parts,PHI,DELPHI,dt):
    # Mapping from Nodes back down to Particles
    # NOTE: nzm means "non zero mass"
    nzmi, = numpy.nonzero(nodes.m)                              # <finding indicies nonzero-mass nodes>
    nzmNodeVel = (nodes.q[nzmi]/nodes.m[nzmi])                  # <Calculating the velocities of nonzero-mass nodes>

    INVPHI = linalg.pinv(PHI)
    INVDELPHI = linalg.pinv(DELPHI[nzmi,:])

    CORPHI = cornerPhi(nodes,parts)                             # <mapping matrix for particle corners>
    INVCORPHI = linalg.pinv(CORPHI[nzmi,:])

    parts.delv[:] = INVDELPHI.dot(nzmNodeVel)                  # Equation (11)
    parts.q[:] = parts.q + INVPHI.dot(nodes.dqdt)*dt           # Equation (12)

    parts.corners[:] = parts.corners + INVCORPHI.dot(nzmNodeVel)*dt   # CPDI-2 Equation (31)
    parts.x = 0.5*(parts.corner(0)+parts.corner(1))             # particle centers set to average of the corners
    parts.defgrad[:] = parts.volume()/parts.restlength          # Modified from CPDI-1's Equation (14,15) to account for CPDI-2

