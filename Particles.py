# Python implementation of CPDI
# Steven Schmidt
# December 2013

import numpy

class Particles():

    def __init__(self,partsxmin,partsxmax,nparts):
        self.nparts = nparts
        self.restlength = (partsxmax - partsxmin)/nparts
        self.x = numpy.arange(partsxmin+(self.restlength/2), partsxmax+(self.restlength/2), self.restlength) # particle centers
        self.corners = numpy.linspace(partsxmin,partsxmax,nparts+1)  # particle corners x-locations (without repeat)
        self.q = numpy.zeros((nparts))  # particle momentum
        self.m = numpy.zeros((nparts))  # particle mass
        self.stress = numpy.zeros((nparts))
        self.fext = numpy.zeros((nparts))
        self.dqdt = numpy.zeros((nparts))
        self.delv = numpy.zeros((nparts))  # velocity gradient
        self.defgrad = numpy.ones((nparts))

    # returns array of lengths of each particle
    def volume(self):
        return self.corners[1:self.nparts+1] - self.corners[0:self.nparts]

    # Average length of all the particles
    def avehx(self):
        return self.volume().sum()/self.nparts

    # side 0 is left corner, side 1 is right corner of a particle
    def corner(self,side):
        return self.corners[0+side:self.nparts+side]

    # xmin of full domain of the particles
    def xmin(self):
        return self.corners[0]

    # xmax of full domain of the particles
    def xmax(self):
        return self.corners[-1]

    # Particle-corner basis function centered at particle corner i, given domain grid array x
    def shapef(self,i,x):
        xdom0 = self.corners[numpy.mod((i-1),(self.nparts+1))]
        xdom1 = self.corners[i]
        xdom2 = self.corners[numpy.mod((i+1),(self.nparts+1))]
        leftelbool = numpy.logical_and(numpy.greater(x,xdom0), numpy.less(x,xdom1))
        rightelbool = numpy.logical_and(numpy.greater(x,xdom1), numpy.less(x,xdom2))
        midpointbool = numpy.equal(x,xdom1)
        return (leftelbool*(x - xdom0)/(xdom1-xdom0) + rightelbool*(x-xdom2)/(xdom1-xdom2) + midpointbool*1.0)

    # d/dx of particle-corner basis function for particle corner i, given domain grid array x
    def del_shapef(self,i,x):
        xdom0 = self.corners[numpy.mod((i-1),(self.nparts+1))]
        xdom1 = self.corners[i]
        xdom2 = self.corners[numpy.mod((i+1),(self.nparts+1))]
        leftelbool = numpy.logical_and(numpy.greater(x,xdom0), numpy.less(x,xdom1))
        rightelbool = numpy.logical_and(numpy.greater(x,xdom1), numpy.less(x,xdom2))
        midpointbool = numpy.equal(x,xdom1)
        return (leftelbool/(xdom1-xdom0) + rightelbool/(xdom1-xdom2) + midpointbool/(xdom1-xdom0))

    # Returns a "stair-step" function of the particle value "partval", given domain grid array x
    # (It is "star-step" because the value is constant over each particle domain)
    def stairstepfforvalue(self,partval,x):
        X,PVAL = numpy.meshgrid(x,partval)
        X,LCOR = numpy.meshgrid(x,self.corner(0))
        X,RCOR = numpy.meshgrid(x,self.corner(1))
        MAT = PVAL*numpy.logical_and(numpy.greater_equal(X,LCOR), numpy.less(X,RCOR))
        ANS = MAT.sum(axis=0)
        return ANS