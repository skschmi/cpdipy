# Python implementation of CPDI
# Steven Schmidt
# December 2013

# NOTE: All equation numbers are from the following paper:
#
# "A convected particle domain interpolation technique to extend
# applicability of the material point method for problems
# involving massive deformations"
# by A. Sadeghirad, R. M. Brannon, and J. Burghardt
#
# unless noted "CPDI-2" in which case it's from this paper:
#
# "Second-order convected particle domain interpolation (CPDI2)
# with enrichment for weak discontinuities at material interfaces"
# by A. Sadeghirad, R.M. Brannon, and J.E. Guilkey

### (Should we calculate dqdx indsted of dvdx (for the "part gradv")? Think about that.)

import numpy
from numpy import linalg
import matplotlib
matplotlib.interactive(True)
from matplotlib import pyplot
import Nodes
import Particles
from cpdihelper import *

pton_cpdi = "CPDI"
pton_normalized = "CPDI_"
pton_pinv = "Pseudo-Inverse"

ntop_cpdi1 = "CPDI-1"
ntop_cpdi2 = "CPDI-2"

ptonMethod = pton_normalized
ntopMethod = ntop_cpdi1

### Initial values ###
rho = 0.11
gamma = 1.0
Patm = -1e10 # Pascals
P0 = Patm
beta = 1e5 # used to set initial velocities
#bodyAcceleration = 10000000000.0
bodyAcceleration = 0.0

### Initialize particles ###
parts = Particles.Particles(partsxmin=0.52,
                            partsxmax=1.52,
                            nparts=20)
parts.m[:] = rho*parts.volume()
parts.q[:] = parts.m*(parts.x*beta)
parts.fext[:] = bodyAcceleration*parts.m

### Initialize nodes ###
nodes = Nodes.Nodes(domainxmin=0.0,
                    domainxmax=5.0,
                    nnodes=51)

# Plotting the Shape Functions
plotShapeFunctions(nodes,parts,titlestr="Initial Shape Functions")

# Calculate appropriate Timestep
dt = calcTimeStep(parts=parts,
                  nodes=nodes,
                  wavespeed=numpy.sqrt(numpy.abs(P0/rho)),
                  cflfrac=0.1)

maxsteps = 200




#############################################
## Main algorithm functions: TEST VERSIONS ##
#############################################

def mapParticlesToNodes_PINV_TEST(nodes,parts,PHI,DELPHI,dt):
    # Mapping from Particles to Nodes
    PSPHI = linalg.pinv(PHI.dot(PHI.T)).dot(PHI)
    PSDELPHI = linalg.pinv(DELPHI.dot(DELPHI.T)).dot(DELPHI)

    #Rescaling PSPHI
    #PSPHI = PSPHI*(1./PSPHI.sum())
    ##print "** PHI.sum(): ", PHI.sum(), " PHITEST.sum() (Pseudo-inverse PHI): ", PHITEST.sum()

    nodes.m[:] = PSPHI.dot(parts.m)                               # Equation (1)
    nodes.q[:] = PSPHI.dot(parts.q)                               # Equation (2)
    nodes.volume[:] = PSPHI.dot(parts.volume())                   # Equation (MapVolumeToNodes)
    nodes.fext[:] = PSPHI.dot(parts.fext)                         # Equation (fext)
    nodes.fint[:] = -PSDELPHI.dot((parts.stress*parts.volume()))  # Equation (8)
    nodes.dqdt[:] = nodes.fext + nodes.fint                         # Equation (9)

def mapParticlesToNodes_DirectPinv_TEST(nodes,parts,PHI,DELPHI,dt):
    # Mapping from Particles to Nodes
    PSPHI = linalg.pinv(PHI.T)
    PSDELPHI = linalg.pinv(DELPHI.T)
    print PSPHI.shape

    nodes.m[:] = PSPHI.dot(parts.m)                               # Equation (1)
    nodes.q[:] = PSPHI.dot(parts.q)                               # Equation (2)
    nodes.volume[:] = PSPHI.dot(parts.volume())                   # Equation (MapVolumeToNodes)
    nodes.fext[:] = PSPHI.dot(parts.fext)                         # Equation (fext)
    nodes.fint[:] = -PSDELPHI.dot((parts.stress*parts.volume()))  # Equation (8)
    nodes.dqdt[:] = nodes.fext + nodes.fint                         # Equation (9)

def mapParticlesToNodes_NORM_TEST(nodes,parts,PHI,DELPHI,dt):
    # Mapping from Particles to Nodes
    PHINORM = PHI.copy()
    colsum = PHINORM.sum(axis=1)
    for i in xrange(0,colsum.size):
        if(numpy.abs(colsum[i])>0.0):
            PHINORM[i,:] = PHINORM[i,:]/colsum[i]
    #print colsum

    nodes.m[:] = PHINORM.dot(parts.m)                               # Equation (1)
    nodes.q[:] = PHINORM.dot(parts.q)                               # Equation (2)
    nodes.volume[:] = PHINORM.dot(parts.volume())                   # Equation (MapVolumeToNodes)
    nodes.fext[:] = PHINORM.dot(parts.fext)                         # Equation (fext)
    nodes.fint[:] = -DELPHI.dot((parts.stress*parts.volume()))  # Equation (8)
    nodes.dqdt[:] = nodes.fext + nodes.fint                     # Equation (9)


####################################
# CPDI-1 version of nodes->particles
####################################

def mapNodesToParticles_CPDI_1(nodes,parts,PHI,DELPHI,dt):
    # Mapping from Nodes back down to Particles
    # NOTE: nzm means "non zero mass"
    nzmi, = numpy.nonzero(nodes.m)                              # <finding indicies nonzero-mass nodes>
    nzmNodeVel = (nodes.q[nzmi]/nodes.m[nzmi])                  # <Calculating the velocities of nonzero-mass nodes>
    parts.delv[:] = (DELPHI[nzmi,:].T).dot(nzmNodeVel)          # Equation (11)
    parts.q[:] = parts.q + parts.m*(PHI[nzmi,:].T).dot(nodes.dqdt[nzmi]/nodes.m[nzmi])*dt    # Equation (12)
    parts.x[:] = parts.x + (PHI[nzmi,:].T).dot(nzmNodeVel)*dt   # Equation (13)
    parts.defgrad[:] = (1+parts.delv*dt)*parts.defgrad          # Equation (14)
    partsvol = parts.defgrad*parts.restlength                   # Equation (15)
    parts.corners[0:parts.nparts] = parts.x - 0.5*partsvol      # Set all left-side corner locations
    parts.corners[parts.nparts] = parts.x[-1] + 0.5*partsvol[-1] # Set the last (right-side) corner location


###############################################
## Main algorithm functions: CPDI-2 VERSIONS ##
###############################################

def mapParticlesToNodes_CPDI(nodes,parts,PHI,DELPHI,dt):
    # Mapping from Particles to Nodes
    nodes.m[:] = PHI.dot(parts.m)                               # Equation (1)
    nodes.q[:] = PHI.dot(parts.q)                               # Equation (2)
    nodes.volume[:] = PHI.dot(parts.volume())                   # Equation (MapVolumeToNodes)
    nodes.fext[:] = PHI.dot(parts.fext)                         # Equation (fext)
    nodes.fint[:] = -DELPHI.dot((parts.stress*parts.volume()))  # Equation (8)
    nodes.dqdt[:] = nodes.fext + nodes.fint                     # Equation (9)

def mapNodesToParticles_CPDI_2(nodes,parts,PHI,DELPHI,dt):
    # Mapping from Nodes back down to Particles
    # NOTE: nzm means "non zero mass"
    nzmi, = numpy.nonzero(nodes.m)                              # <finding indicies nonzero-mass nodes>
    nzmNodeVel = (nodes.q[nzmi]/nodes.m[nzmi])                  # <Calculating the velocities of nonzero-mass nodes>
    parts.delv[:] = (DELPHI[nzmi,:].T).dot(nzmNodeVel)          # Equation (11)
    parts.q[:] = parts.q + parts.m*(PHI[nzmi,:].T).dot(nodes.dqdt[nzmi]/nodes.m[nzmi])*dt    # Equation (12)
    CORPHI = cornerPhi(nodes,parts)                             # <mapping matrix for particle corners>
    parts.corners[:] = parts.corners + (CORPHI[nzmi,:].T).dot(nzmNodeVel)*dt   # CPDI-2 Equation (31)
    parts.x = 0.5*(parts.corner(0)+parts.corner(1))             # particle centers set to average of the corners
    parts.defgrad[:] = parts.volume()/parts.restlength          # Modified from CPDI-1's Equation (14,15) to account for CPDI-2




######################
## Main algorithm loop
######################

for step in xrange(0,maxsteps):

    print "Step: ", step, "Nodes Tot q: ", nodes.q.sum(), " Parts Tot q: ", parts.q.sum(), " |||  Nodes Tot m: ", nodes.m.sum(), " Parts Tot m: ", parts.m.sum()

    PHI = phiip(nodes,parts)                                    # <phi_ip matrix>
    DELPHI = del_phiip(nodes,parts)                             # <del_phi_ip matrix>

    # Mapping from Particles to Nodes
    if(ptonMethod==pton_normalized):
        mapParticlesToNodes_NORM_TEST(nodes,parts,PHI,DELPHI,dt) #<---(testing the normalizing-rows method)
    elif(ptonMethod==pton_pinv):
        mapParticlesToNodes_PINV_TEST(nodes,parts,PHI,DELPHI,dt) #<---(testing the pseudo-inverse method)
        ### mapParticlesToNodes_DirectPinv_TEST(nodes,parts,PHI,DELPHI,dt) #<---(testing the direct-pseudo-inverse method)
    elif(ptonMethod==pton_cpdi):
        mapParticlesToNodes_CPDI(nodes,parts,PHI,DELPHI,dt)    # <----- the regular cpdi-2 set-up


    # Integrating in time on the nodes
    nodes.q[:] = nodes.q + nodes.dqdt*dt                        # Equation (10)

    # Mapping from Nodes back down to Particles
    if(ntopMethod==ntop_cpdi2):
        mapNodesToParticles_CPDI_2(nodes,parts,PHI,DELPHI,dt)
    elif(ntopMethod==ntop_cpdi1):
        mapNodesToParticles_CPDI_1(nodes,parts,PHI,DELPHI,dt)


print "Total Recovered Momentum: ", totalRecoveredMomentum(nodes=nodes,parts=parts,h=0.001)

plotVelocity(nodes=nodes,parts=parts,h=0.001)
plotMassDensity(nodes=nodes,parts=parts,h=0.001)
plotMomentumDensity(nodes=nodes,parts=parts,h=0.001)

plotMassWRONG(nodes=nodes,parts=parts,h=0.001)
plotMomentumWRONG(nodes=nodes,parts=parts,h=0.001)
plotVolumeWRONG(nodes=nodes,parts=parts,h=0.001)

thetitle = "Shape Functions after " + str(maxsteps) + " steps"
plotShapeFunctions(nodes,parts,titlestr=thetitle)

nzmi, = numpy.nonzero(nodes.m)
nzmNodeVel = (nodes.q[nzmi]/nodes.m[nzmi])
PHI = phiip(nodes,parts)
DELPHI = del_phiip(nodes,parts)
CORPHI = cornerPhi(nodes,parts)

PSPHI = linalg.pinv(PHI.dot(PHI.T)).dot(PHI)
PSDELPHI = linalg.pinv(DELPHI.dot(DELPHI.T)).dot(DELPHI)




"""
print ""
print "Sum of PHI along each row (for each node):"
for i in xrange(0,PHI.shape[0]):
    print PHI[i,:].sum()

print ""
print "Sum of PHI along each column (for each particle):"
for i in xrange(0,PHI.shape[1]):
    print PHI[:,i].sum()

print ""
print "Sum of Pseudo-Inverse-PHI along each row (for each node):"
for i in xrange(0,PSPHI.shape[0]):
    print PSPHI[i,:].sum()

print ""
print "Sum of Pseudo-Inverse-PHI along each column (for each particle):"
for i in xrange(0,PSPHI.shape[1]):
    print PSPHI[:,i].sum()

print ""
print "Sum of DELPHI along each row (for each node):"
for i in xrange(0,DELPHI.shape[0]):
    print DELPHI[i,:].sum()

print ""
print "Sum of DELPHI along each column (for each particle):"
for i in xrange(0,DELPHI.shape[1]):
    print DELPHI[:,i].sum()

print ""
print "Sum of Pseudo-Inverse-DELPHI along each row (for each node):"
for i in xrange(0,PSDELPHI.shape[0]):
    print PSDELPHI[i,:].sum()

print ""
print "Sum of Pseudo-Inverse-DELPHI along each column (for each particle):"
for i in xrange(0,PSDELPHI.shape[1]):
    print PSDELPHI[:,i].sum()



print ""
print "PHI.sum(): "
print PHI.sum()

print ""
print "PSPHI.sum() (Pseudo-inverse PHI):"
print PSPHI.sum()

print ""
print "DELPHI.sum(): "
print DELPHI.sum()

print ""
print "PSDELPHI.sum() (Pseudo-inverse DELPHI):"
print PSDELPHI.sum()
"""
